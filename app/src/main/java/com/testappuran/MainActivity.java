package com.testappuran;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final int LAYOUT = R.layout.activity_main;

    private LinearLayout rootLayout;
    private TextView textView;
    private ListView listView;
    private Button changeColorBtn, getDataBtn;

    private ItemAdapter<Item> adapter;
    private List<Item> itemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);

        init();
        upDate();
    }

    private void init() {
        changeColorBtn = (Button) findViewById(R.id.changeColorBtn);
        getDataBtn = (Button) findViewById(R.id.getDataBtn);

        rootLayout = (LinearLayout) findViewById(R.id.rootLayout);
        textView = (TextView) findViewById(R.id.textView);
        listView = (ListView) findViewById(R.id.listView);

        changeColorBtn.setOnClickListener(this);
        getDataBtn.setOnClickListener(this);
        adapter = new ItemAdapter<Item>(this, R.id.dateTv, (ArrayList<Item>) itemList);
        listView.setAdapter(adapter);
    }

    public void inProcess() {
        getDataBtn.setEnabled(false);
        upDate();
    }

    public void isDone(String date) {
        getDataBtn.setEnabled(true);
        textView.setText(date);
        new Item(date).save();
    }

    public void upDate() {
        try {
            itemList.clear();
            itemList.addAll(Item.getAllData());
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changeColorBtn:
                rootLayout.setBackgroundColor(new Random().nextInt());
                break;
            case R.id.getDataBtn:
                new RequestHelper(MainActivity.this).get();
                break;
        }
    }
}