package com.testappuran;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class RequestHelper {

    private final String URL = "http://android-logs.uran.in.ua/test.php";

    private final String DATE_PATTERN = "dd.MM.yyyy HH:mm:ss";

    private MainActivity activity;

    private OkHttpClient client = new OkHttpClient();

    public RequestHelper(MainActivity activity) {
        this.activity = activity;
    }

    public void get() {
        new Proceed().execute();
    }

    private String run() throws IOException {
        Request request = new Request.Builder()
                .url(URL)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    private class Proceed extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            activity.inProcess();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return run();
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            activity.isDone(formatResponse(aVoid));
        }
    }

    private String formatResponse(String response) {
        String date;
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN, Locale.getDefault());
        if (response != null) {
            date = dateFormatter.format(new Date(Integer.parseInt(response) * 1000L));
            return date;
        } else {
            return "ERROR!";
        }
    }
}
