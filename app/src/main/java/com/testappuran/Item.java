package com.testappuran;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Item")
public class Item extends Model {

    @Column
    public String date;

    public Item() {
        super();
    }

    public Item(String date) {
        this.date = date;
    }

    public static List<Item> getAllData() {
        return new Select()
                .from(Item.class)
                .orderBy("date" + " DESC")
                .execute();
    }
}